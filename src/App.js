import React from 'react';
import './App.css';
import {BrowserRouter as Router, Switch, Route} from 'react-router-dom';
import Login from './components/login/Login';
import Dashboard from './components/dashboard/Dashboard';
import Task from './components/task/Task';
import Export from './components/admin/Export';
import AddProject from './components/admin/AddProject';
import ProjectList from './components/project/ProjectList';
import ProfileEdition from './components/admin/ProfileEdition';

function App() {
    return (
        <Router>
            <Switch>
                <Route exact path="/" component={Login}/>
                <Route exact path="/login" component={Login}/>
                <Route exact path="/dashboard" component={Dashboard}/>
                <Route exact path="/task" component={Task}/>
                <Route exact path="/export" component={Export}/>
                <Route exact path="/project/add" component={AddProject}/>
                <Route exact path="/projects" component={ProjectList}/>
                <Route exact path="/profile-edition" component={ProfileEdition}/>
            </Switch>
        </Router>
    )
}

export default App;
