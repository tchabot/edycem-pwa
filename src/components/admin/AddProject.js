import React from 'react';
import Drawer from '../menu/Drawer';
import Container from "@material-ui/core/Container";
import TextField from "@material-ui/core/TextField";
import Button from "@material-ui/core/Button";
import Grid from "@material-ui/core/Grid";

import '../../App.css';
import FormControl from "@material-ui/core/FormControl";
import InputLabel from "@material-ui/core/InputLabel";
import Select from "@material-ui/core/Select";
import OutlinedInput from "@material-ui/core/OutlinedInput";

class AddProject extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            redirect: false,
            projectName: '',
            projectStartDate: '',
            nbHours: '',
            job: '',
            isNew: ''
        };
        this.handleInputChange = this.handleInputChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleCancel = this.handleCancel.bind(this);
        this.handleChange = this.handleChange.bind(this);
    }

    styles = {
        inputWrapper: {
            display: 'flex',
            justifyContent: 'center'
        },
        input: {
            width: '100%'
        },
        title: {
            textAlign: 'center'
        },
        submitButton: {
            marginTop: '30px'
        },
        container: {
            display: 'flex',
            flexWrap: 'wrap',
            flexDirection: 'column',
            justifyContent: 'center'
        },
        buttonsWrapper: {
            marginTop: '40px'
        },
        buttons: {
            width: '100%'
        },
        formControl: {
            minWidth: '120px'
        }
    };

    handleChange(event) {
        this.setState({
            isNew: event.target.value
        })
    }

    handleInputChange(event) {
        this.setState({
            [event.target.name]: event.target.value
        })
    }

    handleSubmit(event) {
        event.preventDefault();
    }

    handleCancel() {
        this.props.history.push('/dashboard');
    }

    render() {
        return (
            <div>
                <Drawer/>
                <Container>
                    <div>
                        <h2 style={this.styles.title}>Création d'un projet</h2>
                    </div>
                    <form onSubmit={this.handleSubmit}>
                        <div style={this.styles.container}>
                            <div style={this.styles.inputWrapper}>
                                <TextField
                                    id="outlined-name"
                                    label="Nom du projet"
                                    name="projectName"
                                    value={this.state.projectName}
                                    onChange={this.handleInputChange}
                                    margin="normal"
                                    variant="outlined"
                                    required
                                    style={this.styles.input}
                                />
                            </div>
                            <div style={this.styles.inputWrapper}>
                                <TextField
                                    id="outlined-name"
                                    label="Date début projet"
                                    name="projectStartDate"
                                    value={this.state.projectStartDate}
                                    onChange={this.handleInputChange}
                                    margin="normal"
                                    variant="outlined"
                                    required
                                    style={this.styles.input}
                                />
                            </div>
                            <div style={this.styles.inputWrapper}>
                                <TextField
                                    id="outlined-name"
                                    label="Nombre d'heures"
                                    name="nbHours"
                                    type="number"
                                    value={this.state.nbHours}
                                    onChange={this.handleInputChange}
                                    margin="normal"
                                    variant="outlined"
                                    required
                                    style={this.styles.input}
                                />
                            </div>
                            <div style={this.styles.inputWrapper}>
                                <TextField
                                    id="outlined-name"
                                    label="Métier"
                                    name="job"
                                    value={this.state.job}
                                    onChange={this.handleInputChange}
                                    margin="normal"
                                    variant="outlined"
                                    required
                                    style={this.styles.input}
                                />
                            </div>
                            <div style={this.styles.inputWrapper}>
                                <FormControl style={this.styles.buttons} variant="outlined">
                                    <InputLabel htmlFor="outlined-age-native-simple">
                                        Nouveauté ?
                                    </InputLabel>
                                    <Select
                                        native
                                        value={this.state.isNew}
                                        onChange={this.handleChange}
                                        input={
                                            <OutlinedInput name="age" id="outlined-age-native-simple" />
                                        }
                                    >
                                        <option value="" />
                                        <option value="Oui">Oui</option>
                                        <option value="Non">Non</option>
                                    </Select>
                                </FormControl>
                            </div>
                        </div>
                        <div style={this.styles.buttonsWrapper}>
                            <Grid container spacing={3}>
                                <Grid item xs={6}>
                                    <div>
                                        <Button
                                            style={this.styles.buttons}
                                            onClick={this.handleCancel}
                                            variant="contained"
                                            color="default"
                                        >
                                            Annuler
                                        </Button>
                                    </div>
                                </Grid>
                                <Grid item xs={6}>
                                    <div style={this.styles.buttons}>
                                        <Button style={this.styles.buttons}
                                                type='submit'
                                                variant="contained"
                                                color="primary"
                                        >
                                            Valider
                                        </Button>
                                    </div>
                                </Grid>
                            </Grid>
                        </div>
                    </form>
                </Container>
            </div>
        )
    }
}

export default AddProject;
