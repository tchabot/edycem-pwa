import React from 'react';
import Drawer from '../menu/Drawer';
import TextField from '@material-ui/core/TextField';
import Container from '@material-ui/core/Container';
import Button from '@material-ui/core/Button';
import Grid from '@material-ui/core/Grid';
import Download from './GenerateFile';

import '../../App.css';

class Export extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            startTime: '',
            endTime: ''
        };
        this.handleChange = this.handleChange.bind(this);
        this.handleCancel = this.handleCancel.bind(this);
    }

    styles = {
        title: {
            textAlign: 'center'
        },
        container: {
            display: 'flex',
            flexWrap: 'wrap',
            flexDirection: 'column',
            justifyContent: 'center'
        },
        input: {
            marginTop: '30px',
            display: 'flex',
            justifyContent: 'center',
        },
        buttonsWrapper: {
            marginTop: '40px'
        },
        buttons: {
            width: '100%'
        },
        dateText: {
            width: '240px'
        }
    };

    handleChange(event) {
        this.setState({
            [event.target.name]: event.target.value
        });
    }

    handleCancel() {
        this.props.history.push('/dashboard');
    }

    render() {
        return (
            <div>
                <Drawer/>
                <Container>
                    <div>
                        <h3 style={this.styles.title}>Exporter des données</h3>
                    </div>
                    <div style={this.styles.container}>
                        <div style={this.styles.input}>
                            <TextField
                                id="startTime"
                                label="Date de début"
                                type="date"
                                defaultValue={new Date()}
                                InputLabelProps={{
                                    shrink: true,
                                }}
                                style={this.styles.dateText}
                                onChange={this.handleChange}
                                name="startTime"
                                required
                            />
                        </div>
                        <div style={this.styles.input}>
                            <TextField
                                id="endTime"
                                label="Date de fin"
                                type="date"
                                defaultValue={new Date()}
                                InputLabelProps={{
                                    shrink: true,
                                }}
                                style={this.styles.dateText}
                                onChange={this.handleChange}
                                name="endTime"
                                required
                            />
                        </div>
                    </div>
                    <div style={this.styles.buttonsWrapper}>
                        <Grid container spacing={3}>
                            <Grid item xs={6}>
                                <div>
                                    <Button
                                        style={this.styles.buttons}
                                        onClick={this.handleCancel}
                                        variant="contained"
                                        color="default"
                                    >
                                        Annuler
                                    </Button>
                                </div>
                            </Grid>
                            <Grid item xs={6}>
                                <div>
                                    <Download startTime={this.state.startTime} endTime={this.state.endTime}/>
                                </div>
                            </Grid>
                        </Grid>
                    </div>
                </Container>
            </div>
        )
    }
}

export default Export;
