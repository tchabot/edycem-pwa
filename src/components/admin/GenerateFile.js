import React from "react";
import ReactExport from "react-data-export";
import Button from "@material-ui/core/Button";

const ExcelFile = ReactExport.ExcelFile;
const ExcelSheet = ReactExport.ExcelFile.ExcelSheet;
const ExcelColumn = ReactExport.ExcelFile.ExcelColumn;

const dataSet1 = [
    {
        label: 'E-CFL',
        description: 'Description projet 1',
        timeEstimated: '3h',
        timeSpent: '3h',
        startTime: '10-07-2019 14h00',
        endTime: '10-07-2019 17h00'
    },
    {
        label: 'P-BRE',
        description: 'Description projet 2',
        timeEstimated: '5h',
        timeSpent: '7h',
        startTime: '15-07-2019 8h00',
        endTime: '15-07-2019 15h00'
    }
];

class Download extends React.Component {
    constructor(props) {
        super(props);
    }

    styles = {
        buttons: {
            width: '100%'
        }
    };

    render() {
        return (
            <ExcelFile
                element={<Button style={this.styles.buttons} variant="contained" color="primary">Exporter</Button>}>
                <ExcelSheet data={dataSet1} name="Tâches">
                    <ExcelColumn label="Label" value="label"/>
                    <ExcelColumn label="Description" value="description"/>
                    <ExcelColumn label="Temps estimé" value="timeEstimated"/>
                    <ExcelColumn label="Temps passé" value='timeSpent'/>
                    <ExcelColumn label="Heure de début" value='startTime'/>
                    <ExcelColumn label="Heure de fin" value='endTime'/>
                </ExcelSheet>
            </ExcelFile>
        );
    }
}

export default Download;
