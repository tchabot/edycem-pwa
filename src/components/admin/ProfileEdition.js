import React from 'react';
import Drawer from '../menu/Drawer';
import TextField from "@material-ui/core/TextField";
import {Container} from "@material-ui/core";
import Grid from "@material-ui/core/Grid";
import Button from "@material-ui/core/Button";
import InputLabel from "@material-ui/core/InputLabel";
import Select from "@material-ui/core/Select";
import FormControl from "@material-ui/core/FormControl";
import MenuItem from "@material-ui/core/MenuItem";

class ProfileEdition extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            userId: '',
            firstName: '',
            lastName: '',
            phoneNumber: '',
            email: '',
            password: '',
            role: 0
        };
        this.handleChange = this.handleChange.bind(this);
        this.handleCancel = this.handleCancel.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleRoleChange = this.handleRoleChange.bind(this);
    }

    styles = {
        container: {
            display: 'flex',
            flexWrap: 'wrap',
            justifyContent: 'center',
            flexDirection: 'column'
        },
        title: {
            textAlign: 'center'
        },
        input: {
            width: '70%',
            marginTop: '20px'
        },
        inputWrapper: {
            display: 'flex',
            justifyContent: 'center',
            flexDirection: 'column'
        },
        buttonsWrapper: {
            marginTop: '40px'
        },
        buttons: {
            width: '100%'
        },
        top: {
            marginTop: '10px'
        },
        top30: {
            marginTop: '30px'
        }
    };

    handleChange(event) {
        this.setState({
            [event.target.name]: event.target.value
        })
    }

    handleRoleChange(event) {
        this.setState({
            role: event.target.value
        })
    }

    handleCancel() {
        this.props.history.push('/dashboard');
    }

    handleSubmit(event) {
        event.preventDefault();
        let currentComponent = this;
        let token = localStorage.getItem('Authorization');

        fetch('http://localhost:8080/user', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': token
            },
            body: JSON.stringify({
                first_name: currentComponent.state.firstName,
                last_name: currentComponent.state.lastName,
                phone_id: currentComponent.state.userId,
                phone_number: currentComponent.state.phoneNumber,
                role_id : parseInt(currentComponent.state.role),
                email: currentComponent.state.email,
                password: currentComponent.state.password,
            })
        }).then((response) => {
            if (response.status >= 400) {
                throw new Error("Bad response from server");
            }
            currentComponent.props.history.push('/dashboard');
            return response;
        }).catch(function (err) {
            console.log(err)
        });
    }

    render() {
        return (
            <div>
                <Drawer/>
                <Container>
                    <div className='title' style={this.styles.title}>
                        <h3>Création utilisateur</h3>
                    </div>
                    <form onSubmit={this.handleSubmit}>
                        <div style={this.styles.inputWrapper}>
                            <FormControl style={this.styles.top}>
                                <InputLabel required htmlFor="role">Rôle</InputLabel>
                                <Select
                                    value={this.state.role}
                                    onChange={this.handleRoleChange}
                                >
                                    <MenuItem value="0">Défaut</MenuItem>
                                    <MenuItem value="1">Administrateur</MenuItem>
                                </Select>
                            </FormControl>
                            <TextField
                                id="userId"
                                name='userId'
                                label="Identifiant"
                                type="text"
                                variant="outlined"
                                value={this.state.userId}
                                onChange={this.handleChange}
                                style={this.styles.top}
                                required
                            />
                            <TextField
                                id="firstName"
                                name='firstName'
                                label="Prénom"
                                type="text"
                                variant="outlined"
                                value={this.state.firstName}
                                onChange={this.handleChange}
                                style={this.styles.top}
                                required
                            />
                            <TextField
                                id="lastName"
                                name='lastName'
                                label="Nom"
                                type="text"
                                variant="outlined"
                                value={this.state.lastName}
                                onChange={this.handleChange}
                                style={this.styles.top}
                                required
                            />
                            <TextField
                                id="phoneNumber"
                                name='phoneNumber'
                                label="Tél"
                                type="number"
                                variant="outlined"
                                value={this.state.phoneNumber}
                                onChange={this.handleChange}
                                style={this.styles.top}
                                required
                            />
                            <TextField
                                id="email"
                                name='email'
                                label="Email"
                                type="email"
                                variant="outlined"
                                value={this.state.email}
                                onChange={this.handleChange}
                                style={this.styles.top}
                                required
                            />
                            <TextField
                                id="password"
                                name='password'
                                label="Mot de passe"
                                type="password"
                                variant="outlined"
                                value={this.state.password}
                                onChange={this.handleChange}
                                style={this.styles.top}
                                required
                            />
                            <div style={this.styles.top30}>
                                <Grid container spacing={3}>
                                    <Grid item xs={6}>
                                        <div style={this.styles.buttons}>
                                            <Button style={this.styles.buttons} onClick={this.handleCancel}
                                                    variant="contained" color="default">
                                                Annuler
                                            </Button>
                                        </div>
                                    </Grid>
                                    <Grid item xs={6}>
                                        <div style={this.styles.buttons}>
                                            <Button style={this.styles.buttons} type="submit"
                                                    variant="contained" color="primary">
                                                Valider
                                            </Button>
                                        </div>
                                    </Grid>
                                </Grid>
                            </div>
                        </div>
                    </form>
                </Container>
            </div>
        );
    }
}

export default ProfileEdition;
