import React from 'react';
import Drawer from '../menu/Drawer';
import Button from "@material-ui/core/Button";
import {Container} from "@material-ui/core";

import '../../App.css';

class Dashboard extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            redirect: false
        };
        this.handleProjectList = this.handleProjectList.bind(this);
        this.handleSchedules = this.handleSchedules.bind(this);
        this.handleAddProject = this.handleAddProject.bind(this);
        this.handleExport = this.handleExport.bind(this);
        this.handleProfileEdition = this.handleProfileEdition.bind(this);
    }

    styles = {
        main: {
            display: 'flex',
            flexDirection: 'column'
        },
        buttonContainer: {
            marginTop: '30px'
        },
        button: {
            width: '100%'
        }
    };

    handleProjectList() {
        this.props.history.push('/projects');
    }

    handleAddProject() {
        this.props.history.push('/project/add');
    }

    handleSchedules() {
        this.props.history.push('/task');
    }

    handleExport() {
        this.props.history.push('/export');
    }

    handleProfileEdition() {
        this.props.history.push('/profile-edition');
    }

    render() {
        return (
            <div>
                <Drawer/>
                <Container>
                    <div style={this.styles.main}>
                        <div style={this.styles.buttonContainer}>
                            <Button style={this.styles.button} onClick={this.handleAddProject} variant="outlined" color="primary">
                                Créer un nouveau projet
                            </Button>
                        </div>
                        <div style={this.styles.buttonContainer}>
                            <Button style={this.styles.button} onClick={this.handleSchedules} variant="outlined" color="primary">
                                Saisie des horaires
                            </Button>
                        </div>
                        <div style={this.styles.buttonContainer}>
                            <Button style={this.styles.button} onClick={this.handleProjectList} variant="outlined" color="primary">
                                Liste des projets
                            </Button>
                        </div>
                        <div style={this.styles.buttonContainer}>
                            <Button style={this.styles.button} onClick={this.handleExport} variant="outlined" color="primary">
                                Exporter les données
                            </Button>
                        </div>
                        <div style={this.styles.buttonContainer}>
                            <Button style={this.styles.button} onClick={this.handleProfileEdition} variant="outlined" color="primary">
                                Création utilisateur
                            </Button>
                        </div>
                    </div>
                </Container>
            </div>
        );
    }
}

export default Dashboard;
