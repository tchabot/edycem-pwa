import React from 'react';
import TextField from "@material-ui/core/TextField";
import {Container} from "@material-ui/core";
import Button from "@material-ui/core/Button";
import logo from '../../edycem-logo.png';
import {Redirect} from 'react-router';

import '../../App.css';

class Login extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            username: '',
            password: '',
            email: '',
            redirect: false
        };
        this.handleUsernameChange = this.handleUsernameChange.bind(this);
        this.handlePasswordChange = this.handlePasswordChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.setToken = this.setToken.bind(this);
    }

    styles = {
        container: {
            display: 'flex',
            flexWrap: 'wrap',
        },
        menu: {
            width: 200,
        },
        logo: {
            width: '210px'
        },
        logoContainer: {
            display: 'flex',
            justifyContent: 'center'
        },
        submitButton: {
            display: 'flex',
            justifyContent: 'center',
            marginTop: '25px'
        }
    };

    setToken(idToken) {
        localStorage.setItem('Authorization', idToken);
    }

    handleSubmit(event) {
        event.preventDefault();
        this.setState({
            redirect: true
        });
        let currentComponent = this;

        fetch('http://localhost:8080/auth/login', {
            method: 'POST',
            headers: {'Content-Type': 'application/json'},
            body: JSON.stringify(
                {
                    email: currentComponent.state.email,
                    password: currentComponent.state.password
                }
            )
        })
            .then(res => res.json())
            .then(
                (result) => {
                    currentComponent.setToken(result.Authorization);
                },
                (error) => {
                    console.log(error);
                }
            );
    }

    handleUsernameChange(event) {
        this.setState({
            username: event.target.value,
            email: event.target.value
        })
    }

    handlePasswordChange(event) {
        this.setState({
            password: event.target.value
        })
    }

    render() {
        if (this.state.redirect) {
            return <Redirect to='/dashboard'/>
        }

        return (
            <div>
                <Container>
                    <div style={this.styles.logoContainer}>
                        <img src={logo} style={this.styles.logo} alt="logo-edycem"/>
                    </div>
                    <form onSubmit={this.handleSubmit}>
                        <div style={this.styles.logoContainer}>
                            <TextField
                                id="outlined-name"
                                label="Identifiant"
                                value={this.state.username}
                                onChange={this.handleUsernameChange}
                                margin="normal"
                                variant="outlined"
                                required
                            />
                        </div>
                        <div style={this.styles.logoContainer}>
                            <TextField
                                id="outlined-password-input"
                                label="Mot de passe"
                                type="password"
                                autoComplete="current-password"
                                value={this.state.password}
                                onChange={this.handlePasswordChange}
                                margin="normal"
                                variant="outlined"
                                required
                            />
                        </div>
                        <div style={this.styles.submitButton}>
                            <Button type='submit' variant="contained" color="primary">
                                Se connecter
                            </Button>
                        </div>
                    </form>
                </Container>
            </div>
        )
    }
}

export default Login;
