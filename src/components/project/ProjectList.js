import React from 'react';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import Drawer from '../menu/Drawer';
import Container from '@material-ui/core/Container';
import Button from '@material-ui/core/Button';

import '../../App.css';

class ProjectList extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            projects: []
        };
        this.handleCancel = this.handleCancel.bind(this);
    }

    styles = {
        root: {
            width: '100%',
            overflowX: 'auto',
            marginTop: '40px'
        },
        table: {
            minWidth: 650,
        },
        title: {
            textAlign: 'center',
            fontFamily: 'Montserrat'
        },
        buttonWrapper: {
            display: 'flex',
            justifyContent: 'center',
            marginTop: '50px'
        },
        buttons: {
            width: '100%'
        }
    };

    createData(job, project, startDate, newOne) {
        return {job, project, startDate, newOne};
    }

    handleCancel() {
        this.props.history.push('/dashboard');
    }

    componentDidMount() {
        let currentComponent = this;
        let token = localStorage.getItem('Authorization');

        fetch('http://localhost:8080/project?page=5&limit=5', {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': token
            },
        })
            .then(res => res.json())
            .then(
                (result) => {
                    if (result.rows) {
                        return currentComponent.setState({projects: result.rows});
                    }
                },
                (error) => {
                    console.log(error);
                }
            );
    }

    render() {
        return (
            <div>
                <Drawer/>
                <div style={this.styles.title}>
                    <h3>Liste des projets</h3>
                </div>
                <Container>
                    <Paper style={this.styles.root}>
                        <Table style={this.styles.table}>
                            <colgroup>
                                <col style={{width: '20%'}}/>
                                <col style={{width: '30%'}}/>
                                <col style={{width: '30%'}}/>
                                <col style={{width: '30%'}}/>
                            </colgroup>
                            <TableHead>
                                <TableRow>
                                    <TableCell>Métier</TableCell>
                                    <TableCell>Projet</TableCell>
                                    <TableCell>Date début</TableCell>
                                    <TableCell>Date fin</TableCell>
                                </TableRow>
                            </TableHead>
                            <TableBody>
                                {this.state.projects.map(project => (
                                    <TableRow key={project.label}>
                                        <TableCell component="th" scope="row">
                                            {project.label}
                                        </TableCell>
                                        <TableCell>{project.description}</TableCell>
                                        <TableCell>{project.start}</TableCell>
                                        <TableCell>{project.end}</TableCell>
                                    </TableRow>
                                ))}
                            </TableBody>
                        </Table>
                    </Paper>
                    <div style={this.styles.buttonWrapper}>
                        <Button style={this.styles.buttons} onClick={this.handleCancel} variant="contained"
                                color="default">
                            Retour
                        </Button>
                    </div>
                </Container>
            </div>
        );
    }
}

export default ProjectList;
