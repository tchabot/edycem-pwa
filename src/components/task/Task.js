import React from 'react';
import Drawer from '../menu/Drawer';
import TextField from '@material-ui/core/TextField';
import {Container} from '@material-ui/core';
import TimeInput from 'material-ui-time-picker'
import Button from '@material-ui/core/Button';
import Grid from '@material-ui/core/Grid';
import '../../App.css';
import InputLabel from "@material-ui/core/InputLabel";
import Select from "@material-ui/core/Select";
import OutlinedInput from "@material-ui/core/OutlinedInput";
import FormControl from "@material-ui/core/FormControl";

class Task extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            startTime: new Date(),
            endTime: new Date(),
            userTime: '',
            taskName: '',
            description: '',
            projects: []
        };
        this.handleCancel = this.handleCancel.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleChange = this.handleChange.bind(this);
        this.handleEndTimeChange = this.handleEndTimeChange.bind(this);
        this.handleStartTimeChange = this.handleStartTimeChange.bind(this);
    }

    styles = {
        container: {
            display: 'flex',
            flexWrap: 'wrap',
            justifyContent: 'center',
            flexDirection: 'column'
        },
        title: {
            textAlign: 'center'
        },
        input: {
            width: '70%',
            marginTop: '20px'
        },
        inputWrapper: {
            display: 'flex',
            justifyContent: 'center',
            flexDirection: 'column'
        },
        buttonsWrapper: {
            marginTop: '40px'
        },
        buttons: {
            width: '100%'
        },
        top: {
            marginTop: '20px'
        }
    };

    componentDidMount() {
        let currentComponent = this;
        let token = localStorage.getItem('Authorization');

        fetch('http://localhost:8080/project?page=5&limit=5', {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': token
            },
        })
            .then(res => res.json())
            .then(
                (result) => {
                    if (result.rows) {
                        return currentComponent.setState({projects: result.rows});
                    }
                },
                (error) => {
                    console.log(error);
                }
            );
    }

    handleSubmit(event) {
        event.preventDefault();

        if (!this.state.priority) {
            this.setState({
                hasError: true
            });
        } else {
            let currentComponent = this;
            let token = localStorage.getItem('Authorization');

            fetch('http://localhost:8080/task', {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': token
                },
                body: JSON.stringify({
                    label: currentComponent.state.label,
                    description: currentComponent.state.description,
                    goal: currentComponent.state.time,
                    timeUsed: '',
                    start: '',
                    end: ''
                })
            }).then((response) => {
                if (response.status >= 400) {
                    throw new Error("Bad response from server");
                }
                currentComponent.setState({
                    taskName: '',
                    priority: ''
                });
                currentComponent.props.history.push('/dashboard');
                return response;
            }).catch(function (err) {
                console.log(err)
            });
        }
    }

    handleCancel() {
        this.props.history.push('/dashboard');
    }

    handleStartTimeChange(time) {
        this.setState({
            startTime: time
        })
    }

    handleEndTimeChange(time) {
        this.setState({
            endTime: time
        })
    }

    handleChange(event) {
        this.setState({
            [event.target.name]: event.target.value
        });
    }

    render() {
        return (
            <div>
                <Drawer/>
                <Container>
                    <div className='title' style={this.styles.title}>
                        <h3>Saisie temps de travail</h3>
                    </div>
                    <form noValidate onSubmit={this.handleSubmit}>
                        <div style={this.styles.container}>
                            <div style={this.styles.inputWrapper}>
                                <FormControl style={this.styles.top}>
                                    <InputLabel htmlFor="projectName">Projet</InputLabel>
                                    <Select
                                        native
                                        value={this.state.projectName}
                                        onChange={this.handleChange}
                                        input={<OutlinedInput name="projectName" id="projectName"/>}
                                        required
                                    >
                                        <option value=""/>
                                        {this.state.projects.map((project) => {
                                            return <option key={project.id} value="suivi">{project.label}</option>
                                        })}
                                    </Select>
                                </FormControl>
                                <FormControl style={this.styles.top}>
                                    <InputLabel htmlFor="taskName">Tâche</InputLabel>
                                    <Select
                                        native
                                        value={this.state.taskName}
                                        onChange={this.handleChange}
                                        input={<OutlinedInput name="taskName" id="taskName"/>}
                                        required
                                    >
                                        <option value=""/>
                                    </Select>
                                </FormControl>
                                <TextField
                                    id="outlined-multiline-static"
                                    label="Description"
                                    multiline
                                    rows="3"
                                    value={this.state.description}
                                    margin="normal"
                                    variant="outlined"
                                    onChange={this.handleChange}
                                    required
                                    name='description'
                                />
                                <TextField
                                    id="date"
                                    label="Date"
                                    type="date"
                                    defaultValue={new Date()}
                                    InputLabelProps={{
                                        shrink: true,
                                    }}
                                    style={this.styles.input}
                                    onChange={this.handleChange}
                                    required
                                    name='date'
                                />
                                <Grid container spacing={3}>
                                    <Grid item xs={6} style={this.styles.top}>
                                        <TextField
                                            disabled
                                            id="outlined-disabled"
                                            label="Temps estimé"
                                            defaultValue="5h"
                                            margin="normal"
                                            variant="outlined"
                                            required
                                        />
                                    </Grid>
                                    <Grid item xs={6} style={this.styles.top}>
                                        <TextField
                                            id="outlined-name"
                                            label="Temps réel"
                                            type="number"
                                            value={this.state.userTime}
                                            onChange={this.handleChange}
                                            margin="normal"
                                            variant="outlined"
                                            required
                                            name='userTime'
                                        />
                                    </Grid>
                                </Grid>
                            </div>
                            <Grid container spacing={3}>
                                <Grid item xs={6} style={this.styles.top}>
                                    <div style={this.styles.inputWrapper}>
                                        <small>Heure de début</small>
                                        <TimeInput
                                            mode='24h'
                                            value={this.state.startTime}
                                            onChange={(time) => this.handleStartTimeChange(time)}
                                            style={this.styles.input}
                                            required
                                            name='startTime'
                                        />
                                    </div>
                                </Grid>
                                <Grid item xs={6} style={this.styles.top}>
                                    <div style={this.styles.inputWrapper}>
                                        <small>Heure de fin</small>
                                        <TimeInput
                                            mode='24h'
                                            value={this.state.endTime}
                                            onChange={(time) => this.handleEndTimeChange(time)}
                                            style={this.styles.input}
                                            required
                                            name='endTime'
                                        />
                                    </div>
                                </Grid>
                            </Grid>
                        </div>
                        <div style={this.styles.buttonsWrapper}>
                            <Grid container spacing={3}>
                                <Grid item xs={6}>
                                    <div style={this.styles.buttons}>
                                        <Button style={this.styles.buttons} onClick={this.handleCancel}
                                                variant="contained" color="default">
                                            Annuler
                                        </Button>
                                    </div>
                                </Grid>
                                <Grid item xs={6}>
                                    <div style={this.styles.buttons}>
                                        <Button style={this.styles.buttons} onClick={this.handleSubmit}
                                                variant="contained" color="primary">
                                            Valider
                                        </Button>
                                    </div>
                                </Grid>
                            </Grid>
                        </div>
                    </form>
                </Container>
            </div>
        )
    }
}

export default Task;
